<?php

/**
 * 脚本调度管理常驻脚本
 * 
 * 该脚本功能为：管理所有消息订阅脚本，负责它们的启动和终止。
 * 脚本会动态读取订阅配置信息(配置信息在protected/config/params.php的subscribers中)，
 * 如果配置有更改，则调度管理器也会实时进行更新：比如删除了某一项配置后，对应的正在运行的脚本进程也会随之终止；
 * 如有增加的配置项，脚本调度器在下一轮调度过程中就会尝试为它们创建独立的子进程。调度器在监视的过程中会检查每个脚本的进程实例，
 * 如果有多个相同的脚本正在运行，则会杀死并保留最近运行的那个脚本进程。
 * 
 * 该调度脚本请以全路径方式启动(见@tutorial)，否则无法检测自身是否已有运行副本
 * @tutorial nohup /usr/bin/php /home/tuyou/ykq/scripts/scenario/protected/yiic daemon > /dev/null 2>&1 &
 * @author keluo<pycorvn@yeah.net>
 * @since 2014-12-31
 */
class DaemonCommand extends CConsoleCommand {
    
    private $_startTime; // 脚本启动时间
    private $_runnigScripts = array(); // 当前启动的脚本清单
    
    public function run($args) {
        date_default_timezone_set('Asia/Chongqing');
        $this->printHelp();
        $this->showMessage('initializing script ...');
        
        $this->_startTime = time();
        
        // 检查自身是否正在运行
        ($pid = $this->checkSelfIsRunning()) && die($this->showMessage('already running, pid is: '.$pid));
        
        pcntl_signal(SIGCHLD, SIG_IGN); // 显式声明不关心子进程的结束，防止尸变
        
        // ps -ef | grep defunct | grep -v grep | wc -l 统计僵尸进程数
        for(;;) {
            $scripts = $this->getScripts(); // 动态读取配置，使后来订阅者立即生效
            $this->checkIfKillRunning($scripts);
            
            $this->_runnigScripts = $scripts;
            
            $this->printStatus();
            
            foreach($scripts as $script) {
                $this->checkIsRunning($script) || $this->forkChildProcess($script);
            }
            sleep(5);
        }
    }
    
    /**
     * 打印主进程及子进程运行状态
     */
    protected function printStatus() {
        $message = PHP_EOL.str_repeat('==', 15).' daemon is watching '.str_repeat('==', 15);
        $message .= PHP_EOL.'main pid: '.  getmypid();
        $message .= ', memory used: '.(memory_get_usage(true)/1024/1024).'M';
        $message .= ', started at: '.date('Y-m-d H:i:s', $this->_startTime);
        $message .= PHP_EOL.'current runing scripts:';
        foreach($this->_runnigScripts as $script) {
            $pids = $this->findPids($script);
            $message .= PHP_EOL.'['.($pids ? 'pid: '.$pids[0] : 'awaking...').'] '.$script;
        }
        $message .= PHP_EOL.str_repeat('==', 40);
        $this->showMessage($message);
    }
    
    /**
     * 检查因配置变化决定是否需要终止相应的子进程
     * @param array $scripts
     */
    protected function checkIfKillRunning(array $scripts) {
        $needTokills = array_diff($this->_runnigScripts, $scripts);
        foreach($needTokills as $script) {
            $pids = $this->findPids($script);
            if(!$pids) continue;
            $this->showMessage($script);
            foreach($pids as $pid) {
                shell_exec('kill -9 '.$pid);
                $this->showMessage('pid killed: '.$pid.' ['.$script.']');
            }
        }
    }
    
    /**
     * 检查自身是否已有正在运行的进程
     * @return boolean
     */
    protected function checkSelfIsRunning() {
        $script = dirname(__DIR__).'/yiic daemon';
        $cmd =  'ps -ef | awk \'{print $9" "$10"\t"$2}\' | grep "^'.$script.'" | awk \'{print $3}\'';
        $result = shell_exec($cmd);
        if(!$result) return false;
        $pids = explode(PHP_EOL, trim($result));
        foreach($pids as $k=>$pid) {
            if(!is_numeric($pid)) unset($pids[$k]);
        }
        if(count($pids) == 1) return false;
        sort($pids, SORT_NUMERIC);
        return $pids[0];
    }
    
    /**
     * 检查脚本是否在运行
     * @param string $script
     * @return boolean
     */
    protected function checkIsRunning($script) {
        $pids = $this->findPids($script);
        if(!$pids) return false;
        for($i = 0; $i < count($pids) - 1; $i++) { // 干掉历史重复多余进程，只保留最近活跃的一个进程
            shell_exec('kill -9 '.$pids[$i]);
        }
        return true;
    }
    
    /**
     * 查找脚本对应的进程号
     * @param type $script
     * @return array
     */
    protected function findPids($script) {
        $args = explode(' ', trim($script));
        $glues = '';
        for($i = 0; $i < count($args); $i++) {
            $glues[] = '$'.(9+$i);
        }
        $cmd = 'ps -ef | awk \'{print $2" "'.implode('" "', $glues).'}\' | grep " '.$script.'$" | awk \'{print $1}\'';
        $result = shell_exec($cmd);
        if(!$result) return false;
        $pids = explode(PHP_EOL, trim($result));
        foreach($pids as $k=>$pid) {
            if(!is_numeric($pid)) unset($pids[$k]);
        }
        sort($pids, SORT_NUMERIC);
        return $pids;
    }
    
    /**
     * 创建子进程，唤起脚本
     * @param string $script
     * @return type
     */
    protected function forkChildProcess($script) {
        $pid = pcntl_fork();
        if(-1 == $pid) {} elseif($pid) {
            $this->showMessage('[pid:'.$pid.'] script awaked: '.$script);
            /*
             * 等待子进程中断或结束，但这会导致主进程被阻塞，无法对整个订阅脚本进行有效调度管理，
             * 在这里主进程只负责创建子进程调用消息监听脚本，不等待其结束或发生异常，
             * 而监听及重现唤起监听脚本的任务在主进程中专门去完成
             * pcntl_waitpid($pid, $status);
             */
        } else {
            $status = pcntl_exec('/usr/bin/php', explode(' ', $script));
            if(false === $status) {
                // @todo log ...
            }
        }
    }
    
    /**
     * 获取所有消息订阅脚本清单
     * @staticvar array $subscribers
     * @return array
     */
    protected function getScripts() {
        $config = require dirname(__DIR__).'/config/params.php';
        $subscribers = $config['params']['subscribers'];
        $scripts = array();
        foreach($subscribers as $subscriber) {
            foreach($subscriber as $script) {
                if(!strlen($script)) continue;
                $scripts[md5($script)] = $script;
            }
        }
        
        return $scripts;
    }
    
    /**
     * 打印脚本说明文档
     */
    protected function printHelp() {
        $this->showMessage(PHP_EOL.
        "Usage: php yiic daemon".PHP_EOL.PHP_EOL.
        "Description:".PHP_EOL.
        "\t脚本调度管理脚本".PHP_EOL.PHP_EOL.
        str_repeat('==', 40).PHP_EOL);
    }
    
    protected function showMessage($message, $error = false) {
        print('[DAEMON]['.date('Y-m-d H:i:s').'] '.($error ? 'error: ' : '').$message.PHP_EOL);
    }
}